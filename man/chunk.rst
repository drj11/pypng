.. $URL$
.. $Rev$

PNG: Chunk by Chunk
===================

The PNG specification defines 18 chunk types.  This document is intended
to help users who are interested in a particular PNG chunk type.  If you
have a particular PNG chunk type in mind, you can look here to see what
support PyPNG provides for it.

Critical Chunks
---------------

``IHDR``
^^^^^^^^

Generated automatically by PyPNG.  The ``IHDR`` chunk specifies image
size, colour model, bit depth, and interlacing.
PyPNG supports all valid combinations, except for interlacing.
The PNG spec has only 2 values for interlacing and has done for
years: no interlacing, and Adam7.
PyPNG supports either option when reading a PNG file, but
when writing only supports no interlacing.
Versions released prior to 2024 did support interlaced writing,
if you want to poke about in the archives.

``PLTE``
^^^^^^^^

Correctly handled when a PNG image is read.
For writing, use the ``palette`` argument to the
:meth:`png.Writer` class.
This can work with PNG colour type 3 (palette required), and
other colour types (palette optional).

``IDAT``
^^^^^^^^

Generated automatically from the pixel data presented to PyPNG.
Multiple ``IDAT`` chunks (of bounded size) can be generated by using
``chunk_limit`` argument to the :meth:`png.Writer` class.

``IEND``
^^^^^^^^

Generated automatically.


Ancillary Chunks
----------------

``tRNS``
^^^^^^^^

When writing,
generated for most colour types when the ``transparent`` argument is
supplied to the :meth:`png.Writer` to specify a transparent colour.  For
colour type 3, colour mapped images, a ``tRNS`` chunk will be generated
automatically from the ``palette`` argument when a palette with alpha
(opacity) values is supplied.

When reading will be processed into the alpha channel (if the
appropriate method is used).

``cHRM``
^^^^^^^^

Ignored when reading.  Not generated.

``gAMA``
^^^^^^^^

When reading a PNG image the ``gAMA`` chunk is converted to a floating
point gamma value; this value is returned in the ``info`` dictionary:
``info['gamma']``.  When writing, the ``gamma`` argument to the
:meth:`png.Writer` class will generate a ``gAMA`` chunk.

Note that there is no code to adjust the pixel values for gamma.
Your code has to do that, if you want it.

``iCCP``
^^^^^^^^

International Color Consortium Profile.

Ignored when reading.  Not generated.

The supported ``prichunkpng`` command line tool can add an ``iCCP``
chunk; and the not so supported ``iccp.py`` script can extract one
from a PNG file and display one in "readable" format.
The not so supported ``mkiccp.py`` script can make certain ICC
Profiles.

``sBIT``
^^^^^^^^

When reading a PNG image the ``sBIT`` chunk will make PyPNG rescale the
pixel values so that they all have the width implied by the ``sBIT``
chunk.  It is possible for a PNG image to have an ``sBIT`` chunk that
specifies 3 different values for the significant bits in each of the 3
colour channels.  In this case PyPNG only uses the largest value.  When
writing a PNG image, an ``sBIT`` chunk will be generated if need
according to the ``bitdepth`` argument specified.  Values other than 1,
2, 4, 8, or 16 will generate an ``sBIT`` chunk, as will values less than
8 for images with more than one plane.

``sRGB``
^^^^^^^^

Ignored when reading.  Not generated.

``tEXt``
^^^^^^^^

Ignored when reading.  Not generated.

``zTXt``
^^^^^^^^

Ignored when reading.  Not generated.

``iTXt``
^^^^^^^^

Ignored when reading.  Not generated.

``bKGD``
^^^^^^^^

When a PNG image is read, a ``bKGD`` chunk will add the ``background``
key to the ``info`` dictionary.  When writing a PNG image, a ``bKGD``
chunk will be generated when the ``background`` argument is used.

``hIST``
^^^^^^^^

Ignored when reading.  Not generated.

``pHYs``
^^^^^^^^

When a PNG image is read, a ``pHYs`` chunk will add the ``physical`` key to
the ``info`` dictionary.
When writing a PNG image, a ``pHYs`` chunk will
be generated if ``x_pixels_per_unit`` and ``y_pixels_per_unit``
is not ``None``.
The default is to not write this chunk.

``sPLT``
^^^^^^^^

Ignored when reading.  Not generated.

``tIME``
^^^^^^^^

Ignored when reading.  Not generated.

Non-standard Chunks
-------------------

Generally it is not possible using PyPNG to generate PNG images with
any other chunk types.
When reading a PNG image, processing it using the chunk
interface, ``png.Reader.chunks``, will allow any chunk to be processed
(by user code).
