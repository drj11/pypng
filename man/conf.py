# PyPNG documentation build configuration file, created by
# sphinx-quickstart on Mon Mar 16 13:12:26 2009.
# Then brutally hacked down by drj.
# See http://sphinx.pocoo.org/config.html

# https://docs.python.org/3.11/library/configparser.html
import configparser
import sys
import os

# So that local modules get picked up, and picked up first.
sys.path.insert(0, os.path.abspath("../code"))

# Read some values from setup.cfg
setup_path = "../setup.cfg"
# Setup Configuration
sconf = configparser.ConfigParser()
sconf.read(setup_path)
_Author = sconf["metadata"]["author"]
_Release = sconf["metadata"]["version"]

# General configuration
# ---------------------

extensions = ["sphinx.ext.autodoc"]
templates_path = []
source_suffix = ".rst"
master_doc = "index"
project = "PyPNG"
author = _Author
copyright = f"2009, 2010, 2012, 2016, 2018 to 2024 {_Author}"
release = _Release
version = release[: release.rfind(".")]
language = "en"
today_fmt = "%Y-%m-%d"
exclude_trees = ["build"]

# https://web.archive.org/web/20240205115617/https://www.sphinx-doc.org/en/master/usage/referencing.html#role-any
default_role = "py:obj"


# Options for HTML output
# -----------------------

html_theme = "nature"
html_static_path = []
html_last_updated_fmt = "%Y-%m-%dT%H:%M:%S"
htmlhelp_basename = "PyPNGdoc"


# Options for LaTeX output
# ------------------------

# The paper size ('letter' or 'a4').
latex_paper_size = "a4"

# The font size ('10pt', '11pt' or '12pt').
# latex_font_size = '10pt'

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name,
#  title, author, document class [howto/manual]).
latex_documents = [
    ("index", "PyPNG.tex", "PyPNG Documentation", "David Jones", "manual"),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
# latex_logo = None

# http://sphinx.pocoo.org/ext/autodoc.html?highlight=__init__
autoclass_content = "both"
