.. $URL$
.. $Rev$

Why Use PyPNG?
==============

- PyPNG is pure Python;
- install with pip: ``python -m pip install git+https://gitlab.com/drj11/pypng@pypng-0.20231004.0``;
- install by copying ``code/png.py``;
- read/write all PNG bitdepths (yes, including 16 bit!) and colour modes;
- support for ``sBIT`` chunk;
- support for many other PNG chunk types;
- convert to/from 1-, 2-, 3-, 4- channel NetPBM PAM files (with
  a command line utility);
- fun command line utilities.

Conversion to and from PAM files means that on the command line,
processing PAM files can be mixed with processing PNG files.

Because PyPNG is dedicated to PNG files it may be able to support more
PNG formats and features than general purpose libraries.
